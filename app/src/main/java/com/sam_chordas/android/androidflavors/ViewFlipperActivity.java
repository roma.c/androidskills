package com.sam_chordas.android.androidflavors;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ViewFlipper;
import android.widget.ViewSwitcher;

public class ViewFlipperActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_flipper);
        ViewFlipper simpleViewFlipper = (ViewFlipper) findViewById(R.id
                .simpleViewFlipper); // get the reference of ViewFlipper
        simpleViewFlipper.startFlipping();
        simpleViewFlipper.setFlipInterval(1000);

//        simpleViewFlipper.showNext();
//        simpleViewFlipper.showPrevious();
//        simpleViewFlipper.showNext();
//        simpleViewFlipper.showNext();


    }

}
